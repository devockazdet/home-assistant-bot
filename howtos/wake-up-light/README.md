| header | header |
| ------ | ------ |
| cell | cell |
| cell | cell |# wake-up-light howto

![](showoff01.png)

![](showoff02.png)

# Home-Assistant configuration

I assume you already have a light bulb entity in home-assistant you want to use.

## Creating new script to turn on the light
* Browse to https://your-home-assistant.domain.com/config/script/edit/new
  * Use the following screenshot as an example and adjust:
    * ![](ha-create-script.png)
    * `Entities`: the light you want to be controlled
    * `Service data`: properties, which are supported by your light device.
      * You can get them using the developer tools (https://your-home-assistant.domain.com/developer-tools/state). Search for your light name. The light must be turned on to see the properties.
      * In this example:
        * `brightness: 255`: maximal brightness
        * `transition: 300`: 5 minutes to get from minimal brightness to maximal
        * `color_temp: 454`: warm color temperature
  * Save the script
  * Edit the script again. In last part in the URL will be your script name (`script.some_name`). Note it. We will need it further for the bot configuration.

# home-assistant-bot configuration

* Use `sample-config.yaml` to add the section for triggering the script
  * https://gitlab.com/olegfiksel/home-assistant-bot/-/blob/master/sample-config.yaml#L111

# Triggering

You can use [Reminder Maubot](https://github.com/maubot/reminder) to trigger the wake up light:

```
!remind 07:05 !wakeup schlafzimmer
```

BTW: don't forget to check/set the timezone for the reminder bot (`!remind tz Europe/Berlin`)
